﻿using UnityEngine;
using System.Collections;

public class aboveRegulator : MonoBehaviour {
    public static aboveRegulator instace;
    private Rigidbody2D rigidbody2D;
    public float constantSpeed = 10.0f;
    public Vector2 velocityStorage;
    public float gravityStorage;
    public bool begun = false;
    public GameObject gameBegun;
    public GameObject stopOverworld;
    // Use this for initialization
    void Start () {
        instace = this;
        rigidbody2D = GetComponent<Rigidbody2D>();
        
        gravityStorage = rigidbody2D.gravityScale;
    }
	
	// Update is called once per frame
	void Update () {
        if (Time.time >= 0 && Time.time <= 1.5)
        {
            velocityStorage = rigidbody2D.velocity;
            
        }
        if (Time.time >= 2 && begun == true)
        {
            rigidbody2D.gravityScale = 0;
            rigidbody2D.velocity = rigidbody2D.velocity * 0;
        }
        if(gameBegun.activeSelf == true)
        {
            begun = true;
            rigidbody2D.gravityScale = gravityStorage;
            rigidbody2D.velocity = velocityStorage;
        }
        if (stopOverworld.activeSelf == true)
        {

            rigidbody2D.velocity = 0 * (rigidbody2D.velocity.normalized);
        }
        else
        {
            rigidbody2D.velocity = constantSpeed * (rigidbody2D.velocity.normalized);
        }
            
    }

    void begin()
    {
        
    }

}
