﻿using UnityEngine;
using System.Collections;

public class randomRock : MonoBehaviour {
    public GameObject enemyRcok1;
    public GameObject enemyRcok2;
    public GameObject enemyRcok3;
    public GameObject enemyRcok4;
    public GameObject enemyRcok5;
    public GameObject enemyRcok6;
    public GameObject enemyRcok7;
    public int rockCount = 1;
    public GameObject gameBegun;
    public GameObject easy;
    public GameObject medium;
    public GameObject hard;
    public GameObject impossible;
    private bool startedEasy = true;
    private bool startedMedium = true;
    private bool startedHard = true;
    private bool startedImpossible = true;


    // Use this for initialization
    void Start () {
        InvokeRepeating("spawn", 7, 3.0F);
        InvokeRepeating("spawn", 7, 1.55F);
    }
	
	// Update is called once per frame
	void Update () {
        if(easy.activeSelf == true)
        {
            //Debug.Log(startedEasy);
            if(startedEasy == true)
            {
                CancelInvoke();
                InvokeRepeating("spawn", 7, 3.0F);
                InvokeRepeating("spawn", 7, 1.55F);
                startedMedium = true;
                startedHard = true;
                //Debug.Log("Easy");
                startedImpossible = true;
                startedEasy = false;
            }
            
        }
        else if (medium.activeSelf == true)
        {
            if (startedMedium == true)
            {
                CancelInvoke();
                InvokeRepeating("spawn", 0, 3.0F);
                InvokeRepeating("spawn", 0, 1.55F);
                startedHard = true;
                //Debug.Log("Medium");
                startedEasy = true;
                InvokeRepeating("spawn", 0, 4.0F);
                startedMedium = false;
                startedImpossible = true;
            }

            

        }
        else if (hard.activeSelf == true)
        {

            if (startedHard == true)
            {
                CancelInvoke();
                InvokeRepeating("spawn", 0, 3.0F);
                InvokeRepeating("spawn", 0, 1.55F);
                startedEasy = true;
                //Debug.Log("Hard");
                startedMedium = true;
                InvokeRepeating("spawn", 0, 4.0F);
                InvokeRepeating("spawn", 0, 6.0F);
                startedHard = false;
                startedImpossible = true;
            }

            

        }
        else if (impossible.activeSelf == true)
        {
            if (startedImpossible == true)
            {
                CancelInvoke();
                InvokeRepeating("spawn", 0, 3.0F);
                InvokeRepeating("spawn", 0, 1.55F);
                startedEasy = true;
                //Debug.Log("Impossible");
                startedHard = true;
                InvokeRepeating("spawn", 0, 4.0F);
                InvokeRepeating("spawn", 0, 6.0F);
                InvokeRepeating("spawn", 0, 8.0F);
                startedImpossible = false;
                startedMedium = true;
            }
            

        }


    }

    void spawn()
    {
        //Debug.Log("Spawn");
        if (gameBegun.activeSelf == true)
        {
            switch (rockCount)
            {
                case 1:
                    Instantiate(enemyRcok1, new Vector2(Random.Range(-3, 4), Random.Range(-31, -31)), Quaternion.identity);
                    break;
                case 2:
                    Instantiate(enemyRcok2, new Vector2(Random.Range(-3, 4), Random.Range(-31, -31)), Quaternion.identity);
                    break;
                case 3:
                    Instantiate(enemyRcok3, new Vector2(Random.Range(-3, 4), Random.Range(-31, -31)), Quaternion.identity);
                    break;
                case 4:
                    Instantiate(enemyRcok4, new Vector2(Random.Range(-3, 4), Random.Range(-31, -31)), Quaternion.identity);
                    break;
                case 5:
                    Instantiate(enemyRcok5, new Vector2(Random.Range(-3, 4), Random.Range(-31, -31)), Quaternion.identity);
                    break;
                case 6:
                    Instantiate(enemyRcok6, new Vector2(Random.Range(-3, 4), Random.Range(-31, -31)), Quaternion.identity);
                    break;
                case 7:
                    Instantiate(enemyRcok7, new Vector2(Random.Range(-3, 4), Random.Range(-31, -31)), Quaternion.identity);
                    break;

            }
            rockCount = Random.Range(1, 8);
            //Instantiate(enemyGameObject, new Vector3(Random.Range(6, -13), Random.Range(6, -13), 0), Quaternion.identity);
        }

    }
}
