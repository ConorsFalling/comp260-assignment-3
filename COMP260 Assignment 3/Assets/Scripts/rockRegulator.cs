﻿using UnityEngine;
using System.Collections;

public class rockRegulator : MonoBehaviour {
    public static rockRegulator instace;
    private Rigidbody2D rigidbody2D;
    public float constantSpeed = 10.0f;
    public Vector2 velocityStorage;
    public float gravityStorage;
    public bool begun = false;
    public bool begunMore = true;
    public GameObject gameBegun;
    //DIfficulty
    public GameObject easy;
    public GameObject medium;
    public GameObject hard;
    public GameObject impossible;
    private bool startedEasy = true;
    private bool startedMedium = true;
    private bool startedHard = true;
    private bool startedImpossible = true;

    // Use this for initialization
    void Start () {
        instace = this;
        rigidbody2D = GetComponent<Rigidbody2D>();
        rigidbody2D.gravityScale = -1;
        gravityStorage = rigidbody2D.gravityScale;
        //gameBegun.SetActive(true);

    }
	
	// Update is called once per frame
	void Update () {



        if (easy.activeSelf == true)
        {
            //Debug.Log(startedEasy);
            if (startedEasy == true)
            {
                constantSpeed = 10.0f;
                startedMedium = true;
                startedHard = true;
                startedImpossible = true;
                startedEasy = false;
            }

        }
        else if (medium.activeSelf == true)
        {
            if (startedMedium == true)
            {
                constantSpeed = 13.0f;
                startedHard = true;
                startedEasy = true;
                startedMedium = false;
                startedImpossible = true;
            }



        }
        else if (hard.activeSelf == true)
        {

            if (startedHard == true)
            {
                constantSpeed = 15.0f;
                startedEasy = true;
                startedMedium = true;
                startedHard = false;
                startedImpossible = true;
            }



        }
        else if (impossible.activeSelf == true)
        {
            if (startedImpossible == true)
            {
                constantSpeed = 18.0f;
                startedHard = true;
                startedImpossible = false;
                startedMedium = true;
                startedEasy = true;
            }


        }








        if (Time.time >= 0 && Time.time <= 1.5)
        {
            //Did
            velocityStorage = rigidbody2D.velocity;
            begun = true;

        }
        if (Time.time >= 0.5 && Time.time <= 1)
        {
            rigidbody2D.gravityScale = gravityStorage;
            rigidbody2D.velocity = velocityStorage;
            rigidbody2D.gravityScale = 0;
            rigidbody2D.velocity = rigidbody2D.velocity * 0;
            gameBegun.SetActive(false);
        }
        //Time.time >= 2 && begun == true && Time.time <= 2.1 || 
        if (gameBegun.activeSelf == false && begun == true)
        {
            //Debug.Log("Off");
            rigidbody2D.isKinematic = true;
            rigidbody2D.gravityScale = 0;
                rigidbody2D.velocity = rigidbody2D.velocity * 0;
                begun = false;
                begunMore = true;


        }
        if (gameBegun.activeSelf == true && begunMore == true)
        {
            rigidbody2D.isKinematic = false;
            //Debug.Log("Onned");
            rigidbody2D.gravityScale = gravityStorage;
                rigidbody2D.velocity = velocityStorage;
                begunMore = false;
                begun = true;

        }
        /**if (gameBegun.activeSelf == false && begun == false)
        {
            rigidbody2D.gravityScale = 0;
            rigidbody2D.velocity = rigidbody2D.velocity * 0;

        }**/


        rigidbody2D.velocity = constantSpeed * (rigidbody2D.velocity.normalized);
    }

    void begin()
    {
        
    }

}
