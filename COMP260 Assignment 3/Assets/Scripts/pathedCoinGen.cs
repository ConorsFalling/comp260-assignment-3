﻿using UnityEngine;
using System.Collections;

public class pathedCoinGen : MonoBehaviour {
    public GameObject coin;
    public int coinCount = 0;
    double interval = 0.2;
    double nextTime = 3;
    double intervalStart = 1;
    double nextTimeStart = 3;
    public int numberCoins;
    public GameObject gameBegun;
    // Use this for initialization
    //Difficulty
    public GameObject easy;
    public GameObject medium;
    public GameObject hard;
    public GameObject impossible;


    void Start () {
        numberCoins = Random.Range(2, 5);
    }
	
	// Update is called once per frame
	void Update () {
        /**if (easy.activeSelf == true)
        {

        }else if(medium.activeSelf == true)
        {

        }
        else if (hard.activeSelf == true)
        {

        }
        else if (impossible.activeSelf == true)
        {

        }**/





        if (Time.time >= nextTimeStart)
        {
            numberCoins = Random.Range(2, 5);
            coinCount = 0;
            nextTime = Time.time + 1;
            intervalStart = Random.Range(2, 3);
            nextTimeStart += intervalStart;
            
        }
        if (coinCount < numberCoins)
        {
            if (Time.time >= nextTime)
            {
                coinCount++;
                coinOnPath();
                nextTime += interval;

            }
        }
        

    }


    void coinOnPath()
    {
        if (gameBegun.activeSelf == true)
        {
            Instantiate(coin, new Vector2(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y), Quaternion.identity);
        }
        
        
        
    }
}
