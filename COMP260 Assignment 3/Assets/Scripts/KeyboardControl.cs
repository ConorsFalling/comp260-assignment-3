﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
[RequireComponent(typeof(Rigidbody))]
public class KeyboardControl : MonoBehaviour {

    //declared variables
    private Rigidbody2D rigidbody2D;
    public float speed = 20f;
    public Sprite faceForward;
    public Sprite faceLeft;
    public Sprite faceRight;
    
    public bool control = false;
    public bool spin = false;
    
    public GameObject gameBegun;
    public GameObject stopOverworldOnward;
    public GameObject dangerZone;
    public GameObject replay;
    public GameObject RanRockGen;
    private double controlTime;
    public bool controlSoon = false;
    public Animator player;
    private bool startControl = false;
    public Rigidbody2D resetStarMan;
    public int lives = 3;
    public GameObject life1;
    public GameObject life2;
    public GameObject life3;
    public Button replayButton;
    public GameObject scoreBackground;
    private bool scriptLater = false;

    //Score
    public Text scoreText;
    int scoreCount = 0;
    public Text distance;
    public Text distanceEnd;
    public int distanceCount = 0;
    public Text coinsCollected;
    int coinsCount = 0;
    public Text multiplier;
    int multiplierCount = 1;
    public Text distanceText;
    public Text gameOver;
    public Image distanceBackground;

    bool invoked = false;

    //Difficulty
    public GameObject easy;
    public GameObject medium;
    public GameObject hard;
    public GameObject impossible;
    Collision2D fallStorage;
    bool isRock = false;
    Collision2D[] wallsAndRocks;
    int wallRockCount = 0;

    //Audio
    public AudioClip menuMusic;
    public AudioClip menuMusic2;
    public AudioClip music;
    public AudioClip music2;
    public AudioClip coin;
    public AudioClip hurt1;
    public AudioClip hurt2;
    public AudioClip hurt3;
    int musicPicker;

    //public AudioClip impact;
    //AudioSource audio;
    //public AudioClip scoreClip;
    private AudioSource audio;
    //private AudioSource audio2;
    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        //audio2 = GetComponent<AudioSource>();
        musicPicker = Random.Range(0, 2);
        if(musicPicker == 0)
        {
            //audio.PlayOneShot(music);
        }
        else
        {
            //audio.PlayOneShot(music2);
        }
        
        
        Screen.SetResolution(768, 1366, true);
        rigidbody2D = GetComponent<Rigidbody2D>();
        //rigidbody2D.useGravity = false;
    }
    void moreDistance()
    {
        
            distanceCount++;
        
        

    }
    void FixedUpdate()
    {
        if (distanceCount < 300)
        {
            easy.SetActive(true);
            impossible.SetActive(false);
            hard.SetActive(false);
            medium.SetActive(false);
            multiplierCount = 1;
        }
        else if (distanceCount >= 300 && distanceCount < 600)
        {
            impossible.SetActive(false);
            hard.SetActive(false);
            easy.SetActive(false);
            medium.SetActive(true);
            multiplierCount = 2;
        }
        else if(distanceCount >= 600 && distanceCount < 900) 
        {
            easy.SetActive(false);
            impossible.SetActive(false);
            medium.SetActive(false);
            hard.SetActive(true);
            multiplierCount = 3;
        }
        else if(distanceCount >= 900)
        {
            easy.SetActive(false);
            medium.SetActive(false);
            hard.SetActive(false);
            impossible.SetActive(true);
            multiplierCount = 4;
        }
        
        //Debug.Log(lives);
        if (gameBegun.activeSelf == true)
        {
            if (invoked == false)
            {
                InvokeRepeating("moreDistance", 0, 0.1f);
                invoked = true;
            }
            
            
            distance.text = distanceCount.ToString() + " m";

        }
        if(gameBegun.activeSelf == false && lives == 0 && control == false || gameBegun.activeSelf == true && control == true && lives == 4)
        {
            
            distanceCount = 0;
            if(lives == 4)
            {
                //audio.enabled = false;
                //audio2.PlayOneShot(music);
                //audio.PlayOneShot(music);
                life1.gameObject.SetActive(true);
                life2.gameObject.SetActive(true);
                life3.gameObject.SetActive(true);
                distance.gameObject.SetActive(true);
                distanceBackground.gameObject.SetActive(true);
                lives = 3;
                //audio.enabled = true;
            }
            
            //CancelInvoke();
            
        }
        if (dangerZone.activeSelf == true)
        {
            replay.SetActive(false);
            dangerZone.SetActive(false);
        }
        if (replay.activeSelf == true)
        {
            rigidbody2D.transform.position = new Vector2(resetStarMan.transform.localPosition.x, resetStarMan.transform.localPosition.y);
            control = true;
            dangerZone.SetActive(true);
            gameBegun.SetActive(true);
            spin = false;
            transform.Rotate(Vector2.up);
            //rigidbody2D.transform.rotation = Quaternion.Slerp(0.0, 0.0, 180);
            transform.rotation = Quaternion.Euler(Vector3.down);
            transform.Rotate(0, 0, 180);
            rigidbody2D.gravityScale = 0;
            if (lives == 0)
            {

                coinsCount = 0;
                
                    
                
                
                impossible.SetActive(false);
                scoreBackground.gameObject.SetActive(false);
                lives = 3;
                life1.SetActive(true);
                life2.SetActive(true);
                life3.SetActive(true);
                scoreText.text = distanceCount + " m";
                
                replayButton.gameObject.SetActive(false);
                scoreBackground.gameObject.SetActive(false);
                distance.gameObject.SetActive(true);
                distanceEnd.gameObject.SetActive(false);
                multiplier.gameObject.SetActive(false);
                coinsCollected.gameObject.SetActive(false);
                scoreText.gameObject.SetActive(false);
                gameOver.gameObject.SetActive(false);
                distanceText.gameObject.SetActive(false);
                distance.gameObject.SetActive(true);
                distanceBackground.gameObject.SetActive(true);
                scoreCount = 0;
            }
            
        }
        

            if (control == true)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            //float moveVertical = Input.GetAxis("Vertical");
            Vector3 movement = new Vector3(moveHorizontal, 0.0f, 0.0f);
            rigidbody2D.velocity = movement * speed;

            if (Input.GetKey(KeyCode.A))
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = faceLeft;
            }
            else if (Input.GetKey(KeyCode.D))
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = faceRight;
            }
            else
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = faceForward;
            }
        }
        if (spin == true)
        {
            transform.Rotate(Vector3.forward * -2);

            //
            //if(Time.time % 2 < 1) {
            //  rigidbody2D.AddForce(Vector2.right * 0f);
            //rigidbody2D.AddForce(Vector2.left * 9.8f);
            //}
            //else
            //{
            //  rigidbody2D.AddForce(Vector2.left * 0f);
            //rigidbody2D.AddForce(Vector2.right * 9.8f);
            //}
            //**/
        }else if(spin == false && control == true)
        {
            transform.rotation = Quaternion.Euler(Vector3.down);
            transform.Rotate(0, 0, 180);
        }
        if (stopOverworldOnward.activeSelf == true && controlSoon == false)
        {
            //Debug.Log("Control Option");
            controlTime = Time.time + 3;
            controlSoon = true;
        }

        //Debug.Log(controlTime);
        //Debug.Log(Time.time);
        if (Time.time > controlTime && stopOverworldOnward.activeSelf == true && control == false && startControl == false) 
        {
            //Debug.Log("Control Enabled");
            RanRockGen.GetComponent<randomRock>().enabled = true;
            control = true;
            gameBegun.SetActive(true);
            player.enabled = false;
            startControl = true;
            scriptLater = true;
            RanRockGen.GetComponent<randomRock>().enabled = true;

        }
        

    }

    // Make Collider for hitting walls, that'll make it work
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "coins")
        {
            audio.PlayOneShot(coin);
            Destroy(other.gameObject);
            coinsCount++;
            
        }
        
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        //Debug.Log("HitWall");
        if (other.gameObject.tag == "resetSides" || other.gameObject.tag == "rocks")
        {
            rigidbody2D.gravityScale = 7;

                if (control == true)
                {
                    if (lives > 1)
                    {
                        if (lives == 3)
                        {
                            audio.PlayOneShot(hurt1);
                            life3.SetActive(false);
                            lives--;
                        }
                        else if (lives == 2)
                        {
                            audio.PlayOneShot(hurt2);
                            life2.SetActive(false);
                            lives--;
                        }

                        replay.SetActive(true);

                    }
                    else
                    {
                    //fallStorage = other;
                        audio.PlayOneShot(hurt3);
                        lives--;

                        life1.SetActive(false);
                        replayButton.gameObject.SetActive(true);
                        scoreBackground.gameObject.SetActive(true);
                        distanceEnd.text = distanceCount.ToString() + " m";
                        distanceEnd.gameObject.SetActive(true);
                        multiplier.gameObject.SetActive(true);
                        multiplier.text = "Multiplier: x" + multiplierCount.ToString();

                        coinsCollected.text = "Coins Collected: " + coinsCount.ToString();
                        coinsCollected.gameObject.SetActive(true);
                        scoreCount = (multiplierCount * coinsCount * 100) + distanceCount;
                        scoreText.text = "Final Score: " + scoreCount.ToString();
                        scoreText.gameObject.SetActive(true);
                        gameOver.gameObject.SetActive(true);
                        distanceText.gameObject.SetActive(true);
                        distance.gameObject.SetActive(false);
                        distanceBackground.gameObject.SetActive(false);



                    }


                    gameBegun.SetActive(false);
                    spin = true;
                    control = false;
                    //Debug.Log("HitStuff");
                }
            
            

        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}