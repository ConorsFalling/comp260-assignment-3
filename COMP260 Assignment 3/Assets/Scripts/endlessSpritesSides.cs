﻿using UnityEngine;
using System.Collections;

public class endlessSpritesSides : MonoBehaviour
{
    //private Rigidbody2D rigidbody2D;
    public Transform topImage;
    public Transform middleImage;
    public Transform BottomImage;
    public Transform topImageRight;
    public Transform middleImageRight;
    public Transform BottomImageRight;
    public Transform BottomestImage;
    public Transform BottomestImageRight;
    public Transform BottomesterImage;
    public Transform BottomesterImageRight;
    public int count = 1;
    //public float constantSpeed = 1.0f;

    // Use this for initialization
    void Start()
    {
        //rigidbody2D = GetComponent<Rigidbody2D>();
        //topImage = GetComponent<Transform>();
        //middleImage = GetComponent<Transform>();
        //BottomImage = GetComponent<Transform>();


    }

    // Update is called once per frame
    void Update()
    {


    }

    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "resetSides":
                ResetPosition();
                //Debug.Log("Test");
                break;

        }
        /**if(col.tag == "resetwall")
        {
            Debug.Log("Test");
            ResetPosition();
            
            
        }**/

    }

    public void ResetPosition()
    {
        switch (count)
        {
            case 1:
                topImage.transform.position = new Vector2(topImage.transform.localPosition.x, BottomesterImage.transform.localPosition.y - 10);
                topImageRight.transform.position = new Vector2(topImageRight.transform.localPosition.x, BottomesterImageRight.transform.localPosition.y - 10);
                count = 2;
                break;
            case 2:
                middleImage.transform.position = new Vector2(middleImage.transform.localPosition.x, topImage.transform.localPosition.y - 10);
                middleImageRight.transform.position = new Vector2(middleImageRight.transform.localPosition.x, topImageRight.transform.localPosition.y - 10);
                count = 3;
                break;
            case 3:
                BottomImage.transform.position = new Vector2(BottomImage.transform.localPosition.x, middleImage.transform.localPosition.y - 10);
                BottomImageRight.transform.position = new Vector2(BottomImageRight.transform.localPosition.x, middleImageRight.transform.localPosition.y - 10);
                count = 4;
                break;

            case 4:
                BottomestImage.transform.position = new Vector2(BottomestImage.transform.localPosition.x, BottomImage.transform.localPosition.y - 10);
                BottomestImageRight.transform.position = new Vector2(BottomestImageRight.transform.localPosition.x, BottomImageRight.transform.localPosition.y - 10);
                count = 5;
                break;
            case 5:
                BottomesterImage.transform.position = new Vector2(BottomesterImage.transform.localPosition.x, BottomestImage.transform.localPosition.y - 10);
                BottomesterImageRight.transform.position = new Vector2(BottomesterImageRight.transform.localPosition.x, BottomestImageRight.transform.localPosition.y - 10);
                count = 1;
                break;


        }
    }


}