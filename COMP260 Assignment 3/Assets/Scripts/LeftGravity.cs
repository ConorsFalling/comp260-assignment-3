﻿using UnityEngine;
using System.Collections;

public class LeftGravity : MonoBehaviour {
    private Rigidbody2D rigidbody2D;
    // Use this for initialization

    // Update is called once per frame
    void Update () {
        
    }

    

    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        rigidbody2D.AddForce(Vector2.left * 9.8f);
    }
}
